module V1
	class UsersController < ApplicationController
		
		before_action :authenticate_user, only: [:login]
        before_action :authenticate_request , except: [:login, :create] #login will iteself give the token and create is a new user

		def create
			@user = User.new(user_params)
			if @user.save
				render json: {msg: "Successfully signed up"}
			else
				head(:unprocessable_entity)
			end
		end

        #for attaining token
		def login

	    end

	    def index
        	render json: {users: "Successfully authenticated"}
        end

	    private
	    def user_params
	    	params.require(:user).permit(:email, :password)
	    end
	end
end