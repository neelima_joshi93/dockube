Rails.application.routes.draw do
  #devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :v1 do
  		#resources :users, only: [:create]
  		post 'sign_up', to: 'users#create'
  		get 'sign_in', to: 'users#login'
  		get 'data', to: 'users#index'

  end
end
