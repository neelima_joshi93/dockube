# README

System must have docker installed.

* clone the repository
* Navigate inside the repository
* run: *docker-compose up -d*
* run: *docker-compose run --rm app rails db:create*
* run: *docker-compose run --rm app rails db:migrate*

* login to container rails console using command: *docker exec -it  peer-project_app_1 bash*
* for entering into rails console: *rails c*


# how to sign up 
curl -H "Accept: application/json" -H "Content-Type: application/json" --data-binary '{"user": {"email": "new@yopmail.com", "password": "newuser"}}' localhost:3000/v1/sign_up

* response 
{"msg":"Successfully signed up"}%            


# how to sign in 
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET 'localhost:3000/v1/sign_in?user[email]=new@yopmail.com&user[password]=newuser'

* response 
{"auth_token":"eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0fQ.SGh0wIsllhXK7J3l8LXtZO_duia7nMv5mDNl3oSkZyE","user":{"id":4,"email":"new@yopmail.com"}}


# how to get api data
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0fQ.SGh0wIsllhXK7J3l8LXtZO_duia7nMv5mDNl3oSkZyE" -X GET 'localhost:3000/v1/data'

* response
{"users":"Successfully authenticated"}%   

NOTE: Changes made in the repository from the host machine will automatically refresh the application.




